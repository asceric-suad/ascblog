<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategory\AscCategoryDefinition;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostCategory\AscPostCategoryDefinition;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostComments\AscPostCommentsDefinition;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostTags\AscPostTagsDefinition;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostTranslation\AscPostTranslationDefinition;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\CascadeDelete;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\User\UserDefinition;

class AscPostDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'asc_post';
    }

    public function getCollectionClass(): string
    {
        return AscPostCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscPostEntity::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new DateTimeField('created_at', 'createdAt'))->addFlags(new Required()),
                (new StringField('post_status', 'postStatus'))->addFlags(new Required()),
                (new StringField('comment_status', 'commentStatus'))->addFlags(new Required()),
                (new DateTimeField('updated_at', 'updatedAt')),
                (new StringField('post_type', 'postType'))->addFlags(new Required()),

                (new TranslatedField('postContent'))->addFlags(new Inherited()),
                (new TranslatedField('postTitle'))->addFlags(new Inherited()),
                (new TranslatedField('postExcerpt'))->addFlags(new Inherited()),
                (new TranslatedField('postSlug'))->addFlags(new Inherited()),
                (new TranslatedField('metaTitle'))->addFlags(new Inherited()),
                (new TranslatedField('metaDescription'))->addFlags(new Inherited()),
                (new TranslatedField('keywords'))->addFlags(new Inherited()),
                (new TranslatedField('seoPath'))->addFlags(new Inherited()),

                (new FkField('featured_media_id', 'featuredMediaId', MediaDefinition::class)),
                (new FkField('post_author', 'postAuthor', AscPostDefinition::class))->addFlags(new Required()),

                new OneToOneAssociationField('media', 'featured_media_id', 'id', MediaDefinition::class, false),
                new OneToOneAssociationField('user', 'post_author', 'id', UserDefinition::class, false),
                //new OneToOneAssociationField('category', 'id', 'post_id', AscPostCategoryDefinition::class, false),
                new OneToManyAssociationField('categories', AscPostCategoryDefinition::class, 'post_id', 'id'),
                (new ManyToManyAssociationField('categories', AscCategoryDefinition::class, AscPostCategoryDefinition::class, 'post_id', 'category_id'))
                    ->addFlags(new CascadeDelete(), new Inherited()),
                (new OneToManyAssociationField('postTags', AscPostTagsDefinition::class, 'post_id', 'id')),
                (new OneToManyAssociationField('postComments', AscPostCommentsDefinition::class, 'post_id', 'id')),
                new TranslationsAssociationField(AscPostTranslationDefinition::class, 'asc_post_id'),
            ]
        );
    }
}
