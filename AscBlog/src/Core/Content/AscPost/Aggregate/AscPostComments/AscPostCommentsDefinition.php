<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostComments;

use AscBlog\Core\Content\AscPost\AscPostDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\BoolField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\Language\LanguageDefinition;
use Shopware\Core\System\User\UserDefinition;

class AscPostCommentsDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'asc_post_comments';
    }

    public function getCollectionClass(): string
    {
        return AscPostCommentsCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscPostCommentsEntity::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new DateTimeField('created_at', 'createdAt'))->addFlags(new Required()),
                (new DateTimeField('updated_at', 'updatedAt')),
                new BoolField('active', 'active'),
                (new LongTextField('post_comment', 'postComment'))->addFlags(new Required()),

                (new FkField('language_id', 'languageId', LanguageDefinition::class))->addFlags(new Required()),
                (new FkField('post_id', 'postId', AscPostDefinition::class))->addFlags(new Required()),
                (new FkField('user_id', 'userId', UserDefinition::class))->addFlags(new Required()),

                new OneToOneAssociationField('user', 'user_id', 'id', UserDefinition::class, false),
                new OneToOneAssociationField('post', 'post_id', 'id', AscPostDefinition::class, false),
                //new ManyToOneAssociationField('postComments', 'post_id',  AscPostDefinition::class, 'id',false),
                new OneToOneAssociationField('postComments', 'post_id', 'id', AscPostDefinition::class, false),
            ]
        );
    }
}
