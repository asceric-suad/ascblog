<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostComments;

use AscBlog\Core\Content\AscPost\AscPostEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\Language\LanguageCollection;
use Shopware\Core\System\User\UserEntity;

class AscPostCommentsEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $postId;

    /**
     * @var AscPostEntity
     */
    protected $post;

    /**
     * @var string
     */
    protected $userId;

    /**
     * @var UserEntity
     */
    protected $user;

    /**
     * @var LanguageCollection
     */
    protected $language;

    /**
     * @var bool|null
     */
    protected $active;

    /**
     * @var string
     */
    protected $postComment;

    /**
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @var \DateTimeInterface
     */
    protected $updatedAt;

    /**
     * @var AscPostCommentsCollection
     */
    protected $postComments;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPostId(): string
    {
        return $this->postId;
    }

    /**
     * @param string $postId
     */
    public function setPostId(string $postId): void
    {
        $this->postId = $postId;
    }

    /**
     * @return AscPostEntity
     */
    public function getPost(): AscPostEntity
    {
        return $this->post;
    }

    /**
     * @param AscPostEntity $post
     */
    public function setPost(AscPostEntity $post): void
    {
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     */
    public function setUserId(string $userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }

    /**
     * @return LanguageCollection
     */
    public function getLanguage(): LanguageCollection
    {
        return $this->language;
    }

    /**
     * @param LanguageCollection $language
     */
    public function setLanguage(LanguageCollection $language): void
    {
        $this->language = $language;
    }

    /**
     * @return bool|null
     */
    public function getActive(): ?bool
    {
        return $this->active;
    }

    /**
     * @param bool|null $active
     */
    public function setActive(?bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getPostComment(): string
    {
        return $this->postComment;
    }

    /**
     * @param string $postComment
     */
    public function setPostComment(string $postComment): void
    {
        $this->postComment = $postComment;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getUpdatedAt(): \DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface $updatedAt
     */
    public function setUpdatedAt(\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return AscPostCommentsCollection
     */
    public function getPostComments(): AscPostCommentsCollection
    {
        return $this->postComments;
    }

    /**
     * @param AscPostCommentsCollection $postComments
     */
    public function setPostComments(AscPostCommentsCollection $postComments): void
    {
        $this->postComments = $postComments;
    }
}
