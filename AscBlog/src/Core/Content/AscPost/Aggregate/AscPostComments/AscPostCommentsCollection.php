<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostComments;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscPostCommentsCollection $entity)
 * @method void              set(string $key, AscPostCommentsCollection $entity)
 * @method AscPostCommentsCollection[]    getIterator()
 * @method AscPostCommentsCollection[]    getElements()
 * @method AscPostCommentsCollection|null get(string $key)
 * @method AscPostCommentsCollection|null first()
 * @method AscPostCommentsCollection|null last()
 */
class AscPostCommentsCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscPostCommentsEntity::class;
    }
}
