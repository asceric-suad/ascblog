<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostTranslation;

use AscBlog\Core\Content\AscPost\AscPostDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\AllowHtml;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class AscPostTranslationDefinition extends EntityTranslationDefinition
{
    public function getEntityName(): string
    {
        return 'asc_post_translation';
    }

    public function getCollectionClass(): string
    {
        return AscPostTranslationCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscPostTranslationEntity::class;
    }

    protected function getParentDefinitionClass(): string
    {
        return AscPostDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new LongTextField('post_content', 'postContent'))->addFlags(new Required(), new AllowHtml()),
                (new StringField('post_title', 'postTitle'))->addFlags(new Required()),
                (new LongTextField('post_excerpt', 'postExcerpt'))->addFlags(new AllowHtml()),
                (new StringField('post_slug', 'postSlug'))->addFlags(new Required()),
                (new StringField('meta_title', 'metaTitle')),
                (new LongTextField('meta_description', 'metaDescription')),
                (new LongTextField('keywords', 'keywords')),
                (new StringField('seo_path', 'seoPath')),
            ]
        );
    }
}
