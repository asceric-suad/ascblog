<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostTranslation;

use AscBlog\Core\Content\AscPost\AscPostEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class AscPostTranslationEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $postContent;

    /**
     * @var string
     */
    protected $postTitle;

    /**
     * @var string|null
     */
    protected $postExcerpt;

    /**
     * @var string
     */
    protected $postSlug;

    /**
     * @var string
     */
    protected $postId;

    /**
     * @var AscPostEntity
     */
    protected $post;

    /**
     * @var string|null
     */
    protected $metaTitle;

    /**
     * @var string|null
     */
    protected $metaDescription;

    /**
     * @var string|null
     */
    protected $keywords;

    /**
     * @var string|null
     */
    protected $seoPath;

    /**
     * @return string
     */
    public function getPostContent(): string
    {
        return $this->postContent;
    }

    /**
     * @param string $postContent
     */
    public function setPostContent(string $postContent): void
    {
        $this->postContent = $postContent;
    }

    /**
     * @return string
     */
    public function getPostTitle(): string
    {
        return $this->postTitle;
    }

    /**
     * @param string $postTitle
     */
    public function setPostTitle(string $postTitle): void
    {
        $this->postTitle = $postTitle;
    }

    /**
     * @return string|null
     */
    public function getPostExcerpt(): ?string
    {
        return $this->postExcerpt;
    }

    /**
     * @param string|null $postExcerpt
     */
    public function setPostExcerpt(?string $postExcerpt): void
    {
        $this->postExcerpt = $postExcerpt;
    }

    /**
     * @return string
     */
    public function getPostSlug(): string
    {
        return $this->postSlug;
    }

    /**
     * @param string $postSlug
     */
    public function setPostSlug(string $postSlug): void
    {
        $this->postSlug = $postSlug;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string|null $metaTitle
     */
    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string|null
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * @param string|null $keywords
     */
    public function setKeywords(?string $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string|null
     */
    public function getSeoPath(): ?string
    {
        return $this->seoPath;
    }

    /**
     * @param string|null $seoPath
     */
    public function setSeoPath(?string $seoPath): void
    {
        $this->seoPath = $seoPath;
    }

    /**
     * @return string
     */
    public function getPostId(): string
    {
        return $this->postId;
    }

    /**
     * @param string $postId
     */
    public function setPostId(string $postId): void
    {
        $this->postId = $postId;
    }

    /**
     * @return AscPostEntity
     */
    public function getPost(): AscPostEntity
    {
        return $this->post;
    }

    /**
     * @param AscPostEntity $post
     */
    public function setPost(AscPostEntity $post): void
    {
        $this->post = $post;
    }
}
