<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscPostTranslationCollection $entity)
 * @method void              set(string $key, AscPostTranslationCollection $entity)
 * @method AscPostTranslationCollection[]    getIterator()
 * @method AscPostTranslationCollection[]    getElements()
 * @method AscPostTranslationCollection|null get(string $key)
 * @method AscPostTranslationCollection|null first()
 * @method AscPostTranslationCollection|null last()
 */
class AscPostTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscPostTranslationEntity::class;
    }
}
