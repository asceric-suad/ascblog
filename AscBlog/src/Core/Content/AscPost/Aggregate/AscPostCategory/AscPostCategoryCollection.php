<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostCategory;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscPostCategoryCollection $entity)
 * @method void              set(string $key, AscPostCategoryCollection $entity)
 * @method AscPostCategoryCollection[]    getIterator()
 * @method AscPostCategoryCollection[]    getElements()
 * @method AscPostCategoryCollection|null get(string $key)
 * @method AscPostCategoryCollection|null first()
 * @method AscPostCategoryCollection|null last()
 */
class AscPostCategoryCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscPostCategoryEntity::class;
    }
}
