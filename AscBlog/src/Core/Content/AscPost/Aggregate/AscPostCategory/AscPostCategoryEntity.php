<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostCategory;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategory\AscCategoryEntity;
use AscBlog\Core\Content\AscPost\AscPostEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class AscPostCategoryEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var AscPostEntity
     */
    protected $post;

    /**
     * @var string
     */
    protected $postId;

    /**
     * @var AscCategoryEntity
     */
    protected $category;

    /**
     * @var string
     */
    protected $categoryId;

    /**
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @var \DateTimeInterface|null
     */
    protected $updatedAt;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return AscPostEntity
     */
    public function getPost(): AscPostEntity
    {
        return $this->post;
    }

    /**
     * @param AscPostEntity $post
     */
    public function setPost(AscPostEntity $post): void
    {
        $this->post = $post;
    }

    /**
     * @return string
     */
    public function getPostId(): string
    {
        return $this->postId;
    }

    /**
     * @param string $postId
     */
    public function setPostId(string $postId): void
    {
        $this->postId = $postId;
    }

    /**
     * @return AscCategoryEntity
     */
    public function getCategory(): AscCategoryEntity
    {
        return $this->category;
    }

    /**
     * @param AscCategoryEntity $category
     */
    public function setCategory(AscCategoryEntity $category): void
    {
        $this->category = $category;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }
}
