<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostCategory;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategory\AscCategoryDefinition;
use AscBlog\Core\Content\AscPost\AscPostDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class AscPostCategoryDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'asc_post_category';
    }

    public function getCollectionClass(): string
    {
        return AscPostCategoryCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscPostCategoryEntity::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new DateTimeField('created_at', 'createdAt'))->addFlags(new Required()),
                (new DateTimeField('updated_at', 'updatedAt')),

                (new FkField('post_id', 'postId', AscPostDefinition::class))->addFlags(new Required()),
                (new FkField('category_id', 'categoryId', AscCategoryDefinition::class)),

                new ManyToOneAssociationField('post', 'post_id', AscPostDefinition::class, 'id',false),
                new ManyToOneAssociationField('category', 'category_id',  AscCategoryDefinition::class, 'id',false),
            ]
        );
    }
}
