<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostMedia;

use AscBlog\Core\Content\AscPost\AscPostDefinition;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class AscPostMediaDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'asc_post_media';
    }

    public function getCollectionClass(): string
    {
        return AscPostMediaCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscPostMediaEntity::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new DateTimeField('created_at', 'createdAt'))->addFlags(new Required()),
                (new DateTimeField('updated_at', 'updatedAt')),

                (new FkField('media_id', 'mediaId', MediaDefinition::class)),
                (new FkField('post_id', 'postId', AscPostDefinition::class))->addFlags(new Required()),

                new OneToOneAssociationField('media', 'media_id', 'id', MediaDefinition::class, false),
                new OneToOneAssociationField('post', 'post_id', 'id', AscPostDefinition::class, false),
            ]
        );
    }
}
