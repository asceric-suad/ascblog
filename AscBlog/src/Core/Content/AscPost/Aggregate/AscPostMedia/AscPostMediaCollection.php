<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostMedia;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscPostMediaCollection $entity)
 * @method void              set(string $key, AscPostMediaCollection $entity)
 * @method AscPostMediaCollection[]    getIterator()
 * @method AscPostMediaCollection[]    getElements()
 * @method AscPostMediaCollection|null get(string $key)
 * @method AscPostMediaCollection|null first()
 * @method AscPostMediaCollection|null last()
 */
class AscPostMediaCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscPostMediaEntity::class;
    }
}
