<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscCategoryTranslation;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscCategoryTranslationCollection $entity)
 * @method void              set(string $key, AscCategoryTranslationCollection $entity)
 * @method AscCategoryTranslationCollection[]    getIterator()
 * @method AscCategoryTranslationCollection[]    getElements()
 * @method AscCategoryTranslationCollection|null get(string $key)
 * @method AscCategoryTranslationCollection|null first()
 * @method AscCategoryTranslationCollection|null last()
 */
class AscCategoryTranslationCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscCategoryTranslationEntity::class;
    }
}
