<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscCategoryTranslation;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategory\AscCategoryDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityTranslationDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\AllowHtml;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\StringField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class AscCategoryTranslationDefinition extends EntityTranslationDefinition
{
    public const ENTITY_NAME = 'asc_category_translation';

    public function getEntityName(): string
    {
        return self::ENTITY_NAME;
    }

    public function getCollectionClass(): string
    {
        return AscCategoryTranslationCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscCategoryTranslationEntity::class;
    }

    public function getParentDefinitionClass(): string
    {
        return AscCategoryDefinition::class;
    }

    protected function defineFields(): FieldCollection
    {
        return new FieldCollection([
            (new StringField('category_name', 'categoryName'))->addFlags(new Required()),
            (new StringField('category_slug', 'categorySlug'))->addFlags(new Required()),
            (new StringField('category_description', 'categoryDescription'))->addFlags(new AllowHtml()),
        ]);
    }
}
