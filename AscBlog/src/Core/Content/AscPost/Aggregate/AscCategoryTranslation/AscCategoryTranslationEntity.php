<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscCategoryTranslation;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategory\AscCategoryEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class AscCategoryTranslationEntity extends Entity
{
    use EntityIdTrait;
    /**
     * @var string
     */
    protected $categoryName;

    /**
     * @var string
     */
    protected $categorySlug;

    /**
     * @var string
     */
    protected $categoryDescription;

    /**
     * @var AscCategoryEntity
     */
    protected $category;

    /**
     * @var string
     */
    protected $categoryId;

    /**
     * @return string
     */
    public function getCategoryName(): string
    {
        return $this->categoryName;
    }

    /**
     * @param string $categoryName
     */
    public function setCategoryName(string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    /**
     * @return string
     */
    public function getCategorySlug(): string
    {
        return $this->categorySlug;
    }

    /**
     * @param string $categorySlug
     */
    public function setCategorySlug(string $categorySlug): void
    {
        $this->categorySlug = $categorySlug;
    }

    /**
     * @return string
     */
    public function getCategoryDescription(): string
    {
        return $this->categoryDescription;
    }

    /**
     * @param string $categoryDescription
     */
    public function setCategoryDescription(string $categoryDescription): void
    {
        $this->categoryDescription = $categoryDescription;
    }

    /**
     * @return string
     */
    public function getCategoryId(): string
    {
        return $this->categoryId;
    }

    /**
     * @param string $categoryId
     */
    public function setCategoryId(string $categoryId): void
    {
        $this->categoryId = $categoryId;
    }

    /**
     * @return AscCategoryEntity
     */
    public function getCategory(): AscCategoryEntity
    {
        return $this->category;
    }

    /**
     * @param AscCategoryEntity $category
     */
    public function setCategory(AscCategoryEntity $category): void
    {
        $this->category = $category;
    }
}
