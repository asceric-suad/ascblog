<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostTags;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscPostTagsCollection $entity)
 * @method void              set(string $key, AscPostTagsCollection $entity)
 * @method AscPostTagsCollection[]    getIterator()
 * @method AscPostTagsCollection[]    getElements()
 * @method AscPostTagsCollection|null get(string $key)
 * @method AscPostTagsCollection|null first()
 * @method AscPostTagsCollection|null last()
 */
class AscPostTagsCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscPostTagsEntity::class;
    }
}
