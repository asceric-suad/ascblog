<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscPostTags;

use AscBlog\Core\Content\AscPost\AscPostDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\LongTextField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\ManyToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;
use Shopware\Core\System\Language\LanguageDefinition;

class AscPostTagsDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'asc_post_tags';
    }

    public function getCollectionClass(): string
    {
        return AscPostTagsCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscPostTagsEntity::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new DateTimeField('created_at', 'createdAt'))->addFlags(new Required()),
                (new DateTimeField('updated_at', 'updatedAt')),
                (new LongTextField('tag_name', 'tagName'))->addFlags(new Required()),

                (new FkField('language_id', 'languageId', LanguageDefinition::class))->addFlags(new Required()),
                (new FkField('post_id', 'postId', AscPostDefinition::class))->addFlags(new Required()),

                new OneToOneAssociationField('post', 'post_id', 'id', AscPostDefinition::class, false),
            ]
        );
    }
}
