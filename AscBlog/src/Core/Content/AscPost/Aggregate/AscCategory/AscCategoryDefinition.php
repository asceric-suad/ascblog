<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscCategory;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategoryTranslation\AscCategoryTranslationDefinition;
use Shopware\Core\Content\Media\MediaDefinition;
use Shopware\Core\Content\Seo\SeoUrl\SeoUrlDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\EntityDefinition;
use Shopware\Core\Framework\DataAbstractionLayer\Field\DateTimeField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\FkField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Inherited;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\PrimaryKey;
use Shopware\Core\Framework\DataAbstractionLayer\Field\Flag\Required;
use Shopware\Core\Framework\DataAbstractionLayer\Field\IdField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToManyAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\OneToOneAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslatedField;
use Shopware\Core\Framework\DataAbstractionLayer\Field\TranslationsAssociationField;
use Shopware\Core\Framework\DataAbstractionLayer\FieldCollection;

class AscCategoryDefinition extends EntityDefinition
{
    public function getEntityName(): string
    {
        return 'asc_category';
    }

    public function getCollectionClass(): string
    {
        return AscCategoryCollection::class;
    }

    public function getEntityClass(): string
    {
        return AscCategoryEntity::class;
    }

    protected function defineFields(): FieldCollection
    {

        return new FieldCollection(
            [
                (new IdField('id', 'id'))->addFlags(new Required(), new PrimaryKey()),
                (new DateTimeField('created_at', 'createdAt'))->addFlags(new Required()),
                (new DateTimeField('updated_at', 'updatedAt')),
                (new TranslatedField('categoryName'))->addFlags(new Inherited()),
                (new TranslatedField('categorySlug'))->addFlags(new Inherited()),
                (new TranslatedField('categoryDescription'))->addFlags(new Inherited()),

                (new FkField('media_id', 'mediaId', MediaDefinition::class)),

                new OneToOneAssociationField('media', 'media_id', 'id', MediaDefinition::class, false),
                new OneToManyAssociationField('seoUrls', SeoUrlDefinition::class, 'foreign_key'),
                new TranslationsAssociationField(AscCategoryTranslationDefinition::class, 'asc_category_id'),
            ]
        );
    }
}
