<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscCategory;

use AscBlog\Core\Content\AscPost\Aggregate\AscCategoryTranslation\AscCategoryTranslationCollection;
use AscBlog\Core\Content\AscPost\Aggregate\AscCategoryTranslation\AscCategoryTranslationEntity;
use Shopware\Core\Content\Media\MediaCollection;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;

class AscCategoryEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @var \DateTimeInterface|null
     */
    protected $updatedAt;

    /**
     * @var string|null
     */
    protected $mediaId;

    /**
     * @var MediaEntity|null
     */
    protected $media;

    /**
     * @var AscCategoryTranslationCollection|null
     */
    protected $translations;

    /**
     * @var string|null
     */
    protected $categoryName;

    /**
     * @var string|null
     */
    protected $categorySlug;

    /**
     * @var string|null
     */
    protected $categoryDescription;

    /**
     * @var AscCategoryCollection|null
     */
    protected $categories;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string|null
     */
    public function getMediaId(): ?string
    {
        return $this->mediaId;
    }

    /**
     * @param string|null $mediaId
     */
    public function setMediaId(?string $mediaId): void
    {
        $this->mediaId = $mediaId;
    }

    /**
     * @return MediaEntity|null
     */
    public function getMedia(): ?MediaEntity
    {
        return $this->media;
    }

    /**
     * @param MediaEntity|null $media
     */
    public function setMedia(?MediaEntity $media): void
    {
        $this->media = $media;
    }

    /**
     * @return AscCategoryTranslationCollection|null
     */
    public function getTranslations(): ?AscCategoryTranslationCollection
    {
        return $this->translations;
    }

    /**
     * @param AscCategoryTranslationCollection|null $translations
     */
    public function setTranslations(?AscCategoryTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string|null
     */
    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    /**
     * @param string|null $categoryName
     */
    public function setCategoryName(?string $categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    /**
     * @return string|null
     */
    public function getCategorySlug(): ?string
    {
        return $this->categorySlug;
    }

    /**
     * @param string|null $categorySlug
     */
    public function setCategorySlug(?string $categorySlug): void
    {
        $this->categorySlug = $categorySlug;
    }

    /**
     * @return string|null
     */
    public function getCategoryDescription(): ?string
    {
        return $this->categoryDescription;
    }

    /**
     * @param string|null $categoryDescription
     */
    public function setCategoryDescription(?string $categoryDescription): void
    {
        $this->categoryDescription = $categoryDescription;
    }

    /**
     * @return AscCategoryCollection|null
     */
    public function getCategories(): ?AscCategoryCollection
    {
        return $this->categories;
    }

    /**
     * @param AscCategoryCollection|null $categories
     */
    public function setCategories(?AscCategoryCollection $categories): void
    {
        $this->categories = $categories;
    }
}
