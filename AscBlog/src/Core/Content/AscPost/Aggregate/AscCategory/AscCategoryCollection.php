<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost\Aggregate\AscCategory;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscCategoryCollection $entity)
 * @method void              set(string $key, AscCategoryCollection $entity)
 * @method AscCategoryCollection[]    getIterator()
 * @method AscCategoryCollection[]    getElements()
 * @method AscCategoryCollection|null get(string $key)
 * @method AscCategoryCollection|null first()
 * @method AscCategoryCollection|null last()
 */
class AscCategoryCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscCategoryEntity::class;
    }
}
