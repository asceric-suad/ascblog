<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost;

use AscBlog\Core\Content\AscPost\Aggregate\AscPostCategory\AscPostCategoryCollection;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostComments\AscPostCommentsCollection;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostTags\AscPostTagsCollection;
use AscBlog\Core\Content\AscPost\Aggregate\AscPostTranslation\AscPostTranslationCollection;
use Shopware\Core\Content\Media\MediaEntity;
use Shopware\Core\Framework\DataAbstractionLayer\Entity;
use Shopware\Core\Framework\DataAbstractionLayer\EntityIdTrait;
use Shopware\Core\System\User\UserEntity;

class AscPostEntity extends Entity
{
    use EntityIdTrait;

    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $postAuthor;

    /**
     * @var \DateTimeInterface
     */
    protected $createdAt;

    /**
     * @var string
     */
    protected $postStatus;

    /**
     * @var string
     */
    protected $commentStatus;

    /**
     * @var \DateTimeInterface|null
     */
    protected $updatedAt;

    /**
     * @var string
     */
    protected $postType;

    /**
     * @var string|null
     */
    protected $featuredMediaId;

    /**
     * @var MediaEntity|null
     */
    protected $media;

    /**
     * @var UserEntity
     */
    protected $user;

    /**
     * @var AscPostTranslationCollection|null
     */
    protected $translations;

    /**
     * @var AscPostCategoryCollection|null
     */
    protected $categories;

    /**
     * @var AscPostCommentsCollection
     */
    protected $postComments;

    /**
     * @var AscPostTagsCollection
     */
    protected $postTags;

    /**
     * @var string|null
     */
    protected $postContent;

    /**
     * @var string|null
     */
    protected $postTitle;

    /**
     * @var string|null
     */
    protected $postExcerpt;

    /**
     * @var string|null
     */
    protected $postSlug;

    /**
     * @var string|null
     */
    protected $metaTitle;

    /**
     * @var string|null
     */
    protected $metaDescription;

    /**
     * @var string|null
     */
    protected $keywords;

    /**
     * @var string|null
     */
    protected $seoPath;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPostAuthor(): string
    {
        return $this->postAuthor;
    }

    /**
     * @param string $postAuthor
     */
    public function setPostAuthor(string $postAuthor): void
    {
        $this->postAuthor = $postAuthor;
    }

    /**
     * @return string|null
     */
    public function getPostStatus(): ?string
    {
        return $this->postStatus;
    }

    /**
     * @param string|null $postStatus
     */
    public function setPostStatus(?string $postStatus): void
    {
        $this->postStatus = $postStatus;
    }

    /**
     * @return string
     */
    public function getCommentStatus(): string
    {
        return $this->commentStatus;
    }

    /**
     * @param string $commentStatus
     */
    public function setCommentStatus(string $commentStatus): void
    {
        $this->commentStatus = $commentStatus;
    }

    /**
     * @return string
     */
    public function getPostType(): string
    {
        return $this->postType;
    }

    /**
     * @param string $postType
     */
    public function setPostType(string $postType): void
    {
        $this->postType = $postType;
    }

    /**
     * @return \DateTimeInterface
     */
    public function getCreatedAt(): \DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTimeInterface $createdAt
     */
    public function setCreatedAt(\DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTimeInterface|null $updatedAt
     */
    public function setUpdatedAt(?\DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string|null
     */
    public function getFeaturedMediaId(): ?string
    {
        return $this->featuredMediaId;
    }

    /**
     * @param string|null $featuredMediaId
     */
    public function setFeaturedMediaId(?string $featuredMediaId): void
    {
        $this->featuredMediaId = $featuredMediaId;
    }

    /**
     * @return MediaEntity|null
     */
    public function getMedia(): ?MediaEntity
    {
        return $this->media;
    }

    /**
     * @param MediaEntity|null $media
     */
    public function setMedia(?MediaEntity $media): void
    {
        $this->media = $media;
    }

    /**
     * @return UserEntity
     */
    public function getUser(): UserEntity
    {
        return $this->user;
    }

    /**
     * @param UserEntity $user
     */
    public function setUser(UserEntity $user): void
    {
        $this->user = $user;
    }

    /**
     * @return AscPostCommentsCollection
     */
    public function getPostComments(): AscPostCommentsCollection
    {
        return $this->postComments;
    }

    /**
     * @param AscPostCommentsCollection $postComments
     */
    public function setPostComments(AscPostCommentsCollection $postComments): void
    {
        $this->postComments = $postComments;
    }

    /**
     * @return AscPostTagsCollection
     */
    public function getPostTags(): AscPostTagsCollection
    {
        return $this->postTags;
    }

    /**
     * @param AscPostTagsCollection $postTags
     */
    public function setPostTags(AscPostTagsCollection $postTags): void
    {
        $this->postTags = $postTags;
    }

    /**
     * @return AscPostCategoryCollection|null
     */
    public function getCategories(): ?AscPostCategoryCollection
    {
        return $this->categories;
    }

    /**
     * @param AscPostCategoryCollection|null $categories
     */
    public function setCategories(?AscPostCategoryCollection $categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return AscPostTranslationCollection|null
     */
    public function getTranslations(): ?AscPostTranslationCollection
    {
        return $this->translations;
    }

    /**
     * @param AscPostTranslationCollection|null $translations
     */
    public function setTranslations(?AscPostTranslationCollection $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string|null
     */
    public function getPostContent(): ?string
    {
        return $this->postContent;
    }

    /**
     * @param string|null $postContent
     */
    public function setPostContent(?string $postContent): void
    {
        $this->postContent = $postContent;
    }

    /**
     * @return string|null
     */
    public function getPostTitle(): ?string
    {
        return $this->postTitle;
    }

    /**
     * @param string|null $postTitle
     */
    public function setPostTitle(?string $postTitle): void
    {
        $this->postTitle = $postTitle;
    }

    /**
     * @return string|null
     */
    public function getPostExcerpt(): ?string
    {
        return $this->postExcerpt;
    }

    /**
     * @param string|null $postExcerpt
     */
    public function setPostExcerpt(?string $postExcerpt): void
    {
        $this->postExcerpt = $postExcerpt;
    }

    /**
     * @return string|null
     */
    public function getPostSlug(): ?string
    {
        return $this->postSlug;
    }

    /**
     * @param string|null $postSlug
     */
    public function setPostSlug(?string $postSlug): void
    {
        $this->postSlug = $postSlug;
    }

    /**
     * @return string|null
     */
    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    /**
     * @param string|null $metaTitle
     */
    public function setMetaTitle(?string $metaTitle): void
    {
        $this->metaTitle = $metaTitle;
    }

    /**
     * @return string|null
     */
    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    /**
     * @param string|null $metaDescription
     */
    public function setMetaDescription(?string $metaDescription): void
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string|null
     */
    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    /**
     * @param string|null $keywords
     */
    public function setKeywords(?string $keywords): void
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string|null
     */
    public function getSeoPath(): ?string
    {
        return $this->seoPath;
    }

    /**
     * @param string|null $seoPath
     */
    public function setSeoPath(?string $seoPath): void
    {
        $this->seoPath = $seoPath;
    }
}
