<?php declare(strict_types=1);

namespace AscBlog\Core\Content\AscPost;

use Shopware\Core\Framework\DataAbstractionLayer\EntityCollection;

/**
 * @method void              add(AscPostCollection $entity)
 * @method void              set(string $key, AscPostCollection $entity)
 * @method AscPostCollection[]    getIterator()
 * @method AscPostCollection[]    getElements()
 * @method AscPostCollection|null get(string $key)
 * @method AscPostCollection|null first()
 * @method AscPostCollection|null last()
 */
class AscPostCollection extends EntityCollection
{
    protected function getExpectedClass(): string
    {
        return AscPostEntity::class;
    }
}
