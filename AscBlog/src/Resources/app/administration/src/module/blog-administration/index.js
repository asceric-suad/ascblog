const { Module } = Shopware;

Module.register('blog-administration', {
    type: 'plugin',
    name: 'Asc Blog',
    title: 'AscBlog',
    description: 'AscBlog',
    color: '#62ff80',
    icon: 'default-eye-open',
    entity: 'shop-the-look',
    favicon: 'default-eye-open.png',

    routes: {
        overview: {
            component: 'blog-index',
            path: 'index',
        },
        categories: {
            component: 'blog-categories',
            path: 'categories'
        }
    },

    navigation: [{
        id: 'general-item',
        label: 'AscBlog',
        color: '#00c9db',
        path: 'asc.post.list',
        icon: 'default-documentation-file',
        position: 100
    }]
});
