import template from './list.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('asc-category-list', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            ascCategoryRepository: null,
            bundles: null,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [{
                property: 'categoryName',
                dataIndex: 'categoryName',
                label: 'Category Name',
                allowResize: true,
                primary: true
            }, {
                property: 'createdAt',
                dataIndex: 'createdAt',
                label: 'Date',
                allowResize: true
            }];
        }
    },

    created() {
        this.ascCategoryRepository = this.repositoryFactory.create('asc_category');

        const criteria = new Criteria()
            .addAssociation('translations')

        this.ascCategoryRepository
            .search(criteria, Shopware.Context.api)
            .then((result) => {
                this.bundles = result;
            });
    },

    methods: {

    }
});
