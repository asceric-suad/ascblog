Shopware.Component.extend('asc-category-create', 'asc-category-detail', {
    methods: {
        async onClickSave() {
            this.isLoading = true;
            let category = this.ascCategoryRepository.create()
            category.categoryName = this.$refs.categoryName.value
            category.categorySlug = this.$refs.categorySlug.value
            category.categoryDescription = this.$refs.categoryDescription.value
            category.mediaId = this.mediaId
            let res = await this.ascCategoryRepository.save(category, Shopware.Context.api)
            let parsed = JSON.parse(res.config.data)
            this.categoryId = parsed.id
            this.showSuccessMsg = true
            this.msg = 'Post has been created'
            this.isLoading = false;
            this.$router.push({ name: 'asc.category.detail', params: { id: this.categoryId } });
        },
    }
});
