import '../asc-category/page/asc-category-list';
import '../asc-category/page/asc-category-detail';
import '../asc-category/page/asc-category-create';
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Module.register('asc-category', {
    type: 'plugin',
    name: 'Category',
    title: 'Categories',
    description: 'This is a simple category',
    color: '#62ff80',
    icon: 'default-object-lab-flask',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'asc-category-list',
            path: 'list'
        },
        detail: {
            component: 'asc-category-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'asc.category.list'
            }
        },
        create: {
            component: 'asc-category-create',
            path: 'create',
            meta: {
                parentPath: 'asc.category.list'
            }
        },
    },

    navigation: [{
        label: 'Categories',
        color: '#00c9db',
        path: 'asc.category.list',
        parent: 'asc.post.list',
        icon: 'default-symbol-content',
    }]
});
