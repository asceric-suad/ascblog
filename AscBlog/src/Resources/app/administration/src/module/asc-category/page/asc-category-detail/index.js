import template from './asc-category-detail.html.twig';

const { Component, StateDeprecated } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('asc-category-detail',{
    template,

    data() {
        return {
            isLoading: false,
            category: {
                categoryName: null,
                categorySlug: null,
                categoryDescription: null,
                mediaId: null,
            },
            showErrorMsg: null,
            showSuccessMsg: null,
            mediaId: null,
        }
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    inject: [
        'repositoryFactory'
    ],

    computed: {
        identifier() {
            return this.template && this.template.internalName
                ? this.template.internalName
                : this.$tc('swag-customized-products.create.title');
        },

        languageStore() {
            return StateDeprecated.getStore('language');
        },

        currentUser() {
            return Shopware.State.get('session').currentUser;
        },

        userId() {
            return this.currentUser ? this.currentUser.id : '';
        },

        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        ascCategoryRepository() {
            return this.repositoryFactory.create('asc_category');
        },
    },

    mounted () {

    },

    created() {
        if (this.languageStore.getCurrentId() !== this.languageStore.systemLanguageId) {
            this.languageStore.setCurrentId(this.languageStore.systemLanguageId)
        }
        this.getCategory();
    },

    methods: {
        getCategory() {
            console.log('id', this.$route.params.id)
            const criteria = new Criteria()
                .addAssociation('translations')
                .addFilter(Criteria.equals('id', this.$route.params.id))

            this.ascCategoryRepository
                .search(criteria, Shopware.Context.api)
                .then((response) => {
                    this.category = response[0];
                    //console.log('catValue', response)
                    //console.log('this.post.categories', this.post.categories)
                    //this.post.categories.map(cat => this.categorySelectionValue.push(cat.id))
                });
        },

        onClickSave() {
            this.isLoading = true;
            this.ascCategoryRepository
                .save(this.category, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    this.$router.push({ name: 'asc.category.detail', params: { id: this.post.id } });
                }).catch((exception) => {
                this.isLoading = false;

                this.createNotificationError({
                    title: 'error',
                    message: exception
                });
            });
        },

        onChangeLanguage(languageId) {
            Shopware.StateDeprecated.getStore('language').setCurrentId(languageId);
            this.langId = languageId;
        },

        setMediaItem(item, field) {
            console.log('name: ', item)
            this.mediaRepository.get(item.targetId, Shopware.Context.api).then((media) => {
                this.mediaItem[field] = media
                this.settings[field] = media.id;
            })
                .catch(err => console.log(err))

            this.mediaId = item.targetId
            //this.imageModal = true
            this.disabled = false
            this.getMediaEntity(item.targetId)
            console.log('media_id: ', this.mediaId)
        },

        onRemoveMedia(field) {
            this.mediaItem[field] = null
            this.settings[field] = null
        },

        onDropMedia(mediaItem, field) {
            this.setMediaItem({targetId: mediaItem.id}, field);
        },

        getMediaEntity(id) {
            this.mediaRepository.get(id, Shopware.Context.api).then(res => {
                console.log('MENTITIY: ', res.url)
                this.mediaEntity = res
                this.mediaUrl = res.url
            });
        },
    }
})
