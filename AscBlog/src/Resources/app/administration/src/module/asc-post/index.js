import './page/asc-post-list';
import './page/asc-post-detail';
import './page/asc-post-create';
import deDE from './snippet/de-DE.json';
import enGB from './snippet/en-GB.json';

Shopware.Module.register('asc-post', {
    type: 'plugin',
    name: 'Post',
    title: 'Posts',
    description: 'This is a simple post',
    color: '#62ff80',
    icon: 'default-object-lab-flask',

    snippets: {
        'de-DE': deDE,
        'en-GB': enGB
    },

    routes: {
        list: {
            component: 'asc-post-list',
            path: 'list'
        },
        detail: {
            component: 'asc-post-detail',
            path: 'detail/:id',
            meta: {
                parentPath: 'asc.post.list'
            }
        },
        create: {
            component: 'asc-post-create',
            path: 'create',
            meta: {
                parentPath: 'asc.post.list'
            }
        },
    },

    navigation: [{
        label: 'Posts',
        color: '#00c9db',
        path: 'asc.post.list',
        icon: 'default-documentation-file'
    }, {
        label: 'Posts',
        path: 'asc.post.list',
        parent: 'asc.post.list'
    }]

});
