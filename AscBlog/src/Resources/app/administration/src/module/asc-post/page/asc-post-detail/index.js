import template from './asc-post-detail.html.twig';

const { Component, StateDeprecated } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('asc-post-detail',{
    template,

    data() {
        return {
            item: null,
            settings: {},
            langId: null,
            isLoading: false,
            categories: [
                {
                    postId: null,
                    categoryId: null
                }
            ],
            categorySelectionDropdown: [],
            categorySelectionValue: [],
            filterLoading: false,
            post: {
                id: null,
                postAuthor: null,
                createdAt: null,
                postStatus: null,
                commentStatus: null,
                postType: null,
                featuredMediaId: null,
                categories: {
                    categoryId: null
                },
                translations: {
                    languageId: null,
                    postContent: null,
                    postTitle: null,
                    postExcerpt: null,
                    postSlug: null,
                },
                media: {
                    id: null,
                    postId: null,
                    mediaId: null
                }
            },
            parentPost: {
                media: null,
            },
            media: null,
            translation: {
                postTitle: null,
                postSlug: null,
                postContent: null,
            },
            postTitle: null,
            postId: '',
            showSuccessMsg: false,
            showErrorMsg: false,
            msg: '',
            featuredMediaId: null,
            mediaEntity: null,
            mediaUrl: null,
            disabled: true,
            mediaItem: {
                splashScreen: null,
                appIcon: null,
                appleIcon: null,
                favicon: null
            },
        }
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    inject: [
        'repositoryFactory'
    ],

    computed: {
        identifier() {
            return this.template && this.template.internalName
                ? this.template.internalName
                : this.$tc('swag-customized-products.create.title');
        },

        languageStore() {
            return StateDeprecated.getStore('language');
        },

        currentUser() {
            return Shopware.State.get('session').currentUser;
        },

        userId() {
            return this.currentUser ? this.currentUser.id : '';
        },

        mediaRepository() {
            return this.repositoryFactory.create('media');
        },

        ascPostRepository() {
            return this.repositoryFactory.create('asc_post');
        },

        ascPostTranslationRepository() {
            return this.repositoryFactory.create('asc_post_translation');
        },

        categoryRepository() {
            return this.repositoryFactory.create('asc_category');
        },

        postCatRepository() {
            return this.repositoryFactory.create('asc_post_category');
        },
    },

    mounted () {
        const criteria = new Criteria()
            .addAssociation('translations')
        this.categoryRepository.search(criteria, Shopware.Context.api)
            .then((result) => {

                result.forEach((category) => {
                    console.log('a', category.id)
                    console.log('a', category.categoryName)
                    this.categorySelectionDropdown.push({value: category.id, label: category.categoryName})
                })
            });
    },

    created() {
        if (this.languageStore.getCurrentId() !== this.languageStore.systemLanguageId) {
            this.languageStore.setCurrentId(this.languageStore.systemLanguageId)
        }
        this.getPost();
    },

    methods: {
        getPost() {
            console.log('id', this.$route.params.id)
            const criteria = new Criteria()
                .addAssociation('translations')
                .addAssociation('categories')
                .addFilter(Criteria.equals('id', this.$route.params.id))

            this.ascPostRepository
                .search(criteria, Shopware.Context.api)
                .then((response) => {
                    this.post = response[0];
                    //console.log('catValue', response)
                    //console.log('this.post.categories', this.post.categories)
                    //this.post.categories.map(cat => this.categorySelectionValue.push(cat.id))
                });

            this.categoryRepository.search(new Criteria(), Shopware.Context.api)
                .then((result) => {

                    result.forEach((category) => {
                        console.log('a', category.id)
                        this.categorySelectionValue.push(category.id)
                    })
                });

            //
        },

        onClickSave() {
            this.isLoading = true;

            this.ascPostRepository
                .save(this.post, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    //this.$router.push({ name: 'asc.post.detail', params: { id: this.post.id } });
                }).catch((exception) => {
                this.isLoading = false;

                this.createNotificationError({
                    title: 'error',
                    message: exception
                });
            });

            /*
            this.ascPostTranslationRepository
                .save(this.post.translation, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    this.$router.push({ name: 'asc.post.detail', params: { id: this.post.translation.postId } });
                }).catch((exception) => {
                this.isLoading = false;

                this.createNotificationError({
                    title: this.$tc('asc.post.detail.errorTitle'),
                    message: exception
                });
            });
            */

            this.isLoading = true;
            this.postCatRepository
                .save(this.post.categories, Shopware.Context.api)
                .then(() => {
                    this.isLoading = false;
                    this.$router.push({ name: 'asc.post.detail', params: { id: this.post.categories.postId } });
                }).catch((exception) => {
                this.isLoading = false;

                this.createNotificationError({
                    title: 'cat error',
                    message: exception
                });
            });
        },

        async saveAscPost() {
            try {
                this.isLoading = true;
                let post = this.ascPostRepository.create()
                post.postStatus = this.$refs.postStatus.value
                post.commentStatus = this.$refs.commentStatus.value
                post.postType = this.$refs.postType.value
                post.postAuthor = this.userId
                post.featuredMediaId = this.mediaId
                let res = await this.ascPostRepository.save(post, Shopware.Context.api)
                let parsed = JSON.parse(res.config.data)
                this.postId = parsed.id
                this.savePostCat(this.postId)
                //this.savePostTranslations(this.postId)
                this.isLoading = false;
                this.showSuccessMsg = true
                this.msg = 'Post has been created'

            } catch (error) {
                console.log(error)
                this.isLoading = false;
                this.showErrorMsg = true;
                this.msg = 'There was an issue, post has not been created'
            }
        },

        savePostCat(post_id) {
            try {
                this.categorySelectionValue .forEach(cat => {
                    let postCat = this.postCatRepository.create()
                    postCat.postId = post_id
                    postCat.categoryId = cat
                    this.postCatRepository.save(postCat, Shopware.Context.api)
                })
            } catch (error) {
                console.log(error)
            }
        },

        savePostTranslations(post_id) {
            let translation = this.ascPostTranslationRepository.create()
            translation.postId = post_id
            translation.languageId = this.languageStore.getCurrentId()
            translation.postTitle = this.$refs.postTitle.value
            translation.postSlug = this.$refs.postSlug.value
            translation.postContent = this.$refs.postContent.value
            this.ascPostTranslationRepository.save(translation, Shopware.Context.api)
        },

        onSetSelectedCat(value) {
            this.categorySelectionValue = value
        },

        onChangeLanguage(languageId) {
            Shopware.StateDeprecated.getStore('language').setCurrentId(languageId);
            this.langId = languageId;
        },

        setMediaItem(item, field) {
            console.log('name: ', item)
            this.mediaRepository.get(item.targetId, Shopware.Context.api).then((media) => {
                this.mediaItem[field] = media
                this.settings[field] = media.id;
            })
                .catch(err => console.log(err))

            this.featuredMediaId = item.targetId
            //this.imageModal = true
            this.disabled = false
            this.getMediaEntity(item.targetId)
            console.log('media_id: ', this.featuredMediaId)
        },

        onRemoveMedia(field) {
            this.mediaItem[field] = null
            this.settings[field] = null
        },

        onDropMedia(mediaItem, field) {
            this.setMediaItem({targetId: mediaItem.id}, field);
        },

        getMediaEntity(id) {
            this.mediaRepository.get(id, Shopware.Context.api).then(res => {
                console.log('MENTITIY: ', res.url)
                this.mediaEntity = res
                this.mediaUrl = res.url
            });
        },
    }
})
