Shopware.Component.extend('asc-post-create', 'asc-post-detail', {
    methods: {
        getPost() {
            //this.post = this.ascPostRepository.create(Shopware.Context.api);
        },

        async onClickSave() {
            this.isLoading = true;
            let post = this.ascPostRepository.create()
            post.postTitle = this.$refs.postTitle.value
            post.postSlug = this.$refs.postSlug.value
            post.postContent = this.$refs.postContent.value
            post.postExcerpt = this.$refs.postExcerpt.value
            post.postStatus = this.$refs.postStatus.value
            post.commentStatus = this.$refs.commentStatus.value
            post.postType = this.$refs.postType.value
            post.postAuthor = this.userId
            post.featuredMediaId = this.featuredMediaId
            let res = await this.ascPostRepository.save(post, Shopware.Context.api)
            let parsed = JSON.parse(res.config.data)
            console.log(parsed)
            this.postId = parsed.id
            this.savePostCat(this.postId)
            //this.savePostTranslations(this.postId)
            this.showSuccessMsg = true
            this.msg = 'Post has been created'
            this.isLoading = false;
            //this.$router.push({ name: 'asc.post.detail', params: { id: this.postId } });
        },
    }
});
