import template from './list.html.twig';

const { Component } = Shopware;
const { Criteria } = Shopware.Data;

Component.register('asc-post-list', {
    template,

    inject: [
        'repositoryFactory'
    ],

    data() {
        return {
            ascPostRepository: null,
            bundles: null,
        };
    },

    metaInfo() {
        return {
            title: this.$createTitle()
        };
    },

    computed: {
        columns() {
            return [{
                property: 'postTitle',
                dataIndex: 'postTitle',
                label: 'Title',
                allowResize: true,
                primary: true
            }, {
                property: 'user.username',
                dataIndex: 'user.username',
                label: 'Author',
                allowResize: true
            }, {
                property: 'createdAt',
                dataIndex: 'createdAt',
                label: 'Date',
                allowResize: true
            }];
        }
    },

    created() {
        this.ascPostRepository = this.repositoryFactory.create('asc_post');

        const criteria = new Criteria()
            .addAssociation('translations')
            .addAssociation('user')

        this.ascPostRepository
            .search(criteria, Shopware.Context.api)
            .then((result) => {
                this.bundles = result;
            });
    },

    methods: {

    }
});
