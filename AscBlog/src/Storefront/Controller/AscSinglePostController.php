<?php declare(strict_types=1);

namespace AscBlog\Storefront\Controller;

use Doctrine\DBAL\Connection;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\Account\Login\AccountLoginPageLoader;
use Shopware\Storefront\Page\GenericPageLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\RequestStack;
use Shopware\Storefront\Framework\Cookie\CookieProviderInterface;
use Shopware\Core\Framework\Uuid\Uuid;

class AscSinglePostController extends StorefrontController
{
    /**
     * @var AccountLoginPageLoader
     */
    private $loginPageLoader;

    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var CookieProviderInterface
     */
    private $cookieProvider;

    /**
     * @var GenericPageLoader
     */
    private $pageLoader;

    public function __construct(
        AccountLoginPageLoader $loginPageLoader,
        AccountService $accountService,
        CartService $cartService,
        Connection $connection,
        RequestStack $requestStack,
        CookieProviderInterface $cookieProvider,
        GenericPageLoader $pageLoader
    ) {
        $this->loginPageLoader = $loginPageLoader;
        $this->accountService = $accountService;
        $this->cartService = $cartService;
        $this->connection = $connection;
        $this->requestStack = $requestStack;
        $this->cookieProvider = $cookieProvider;
        $this->pageLoader = $pageLoader;
    }

    /**
     * @RouteScope(scopes={"storefront"})
     * @Route("/blog/{postId}", name="frontend.asc-post", methods={"GET"}, defaults={"XmlHttpRequest": true})
     */
    public function index(Request $request, SalesChannelContext $salesChannelContext): Response
    {
        $page = $this->pageLoader->load($request, $salesChannelContext);
        $postId = $request->attributes->get('postId');
        $languageId = $salesChannelContext->getContext()->getLanguageId();
        error_log(print_r(array('$languageId', $languageId), true)."\n", 3, '/app/error.log');
        $ascPostRepository = $this->container->get('asc_post.repository');
        $criteria = new Criteria();
        $criteria->addAssociation('media')
            ->addAssociation('user')
            ->addAssociation('translations')
            ->addAssociation('category')
            ->addAssociation('category.category')
            ->addAssociation('category.category.translations')
            ->addAssociation('postComments')
            ->addAssociation('postComments.user')
            ->addAssociation('postTags')
            ->addFilter(new EqualsFilter('asc_post.id', $postId));
            //->addFilter(new EqualsFilter('category.category.translations.languageId', $languageId));
        $res = $ascPostRepository->search($criteria, $salesChannelContext->getContext());
        $posts = $res->getEntities();

        return $this->renderStorefront('@Storefront/storefront/page/asc-single-post/index.html.twig', ['page' => $page, 'posts' => $posts]);

    }
}
