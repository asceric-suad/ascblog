<?php

declare(strict_types=1);

namespace AscBlog\Storefront\Controller;

use Doctrine\DBAL\Connection;
use Shopware\Core\Checkout\Customer\Exception\BadCredentialsException;
use Shopware\Core\Checkout\Customer\SalesChannel\AccountService;
use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Shopware\Core\Framework\Routing\Annotation\RouteScope;
use Shopware\Core\Framework\Util\Random;
use Shopware\Core\Framework\Uuid\Uuid;
use Shopware\Core\Framework\Validation\DataBag\RequestDataBag;
use Shopware\Core\System\SalesChannel\SalesChannelContext;
use Shopware\Storefront\Page\Account\Login\AccountLoginPageLoader;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Shopware\Core\Checkout\Cart\SalesChannel\CartService;
use Shopware\Storefront\Controller\StorefrontController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Cookie;
use Shopware\Storefront\Framework\Cookie\CookieProviderInterface;

class DemoDataController extends StorefrontController
{
    /**
     * @var AccountLoginPageLoader
     */
    private $loginPageLoader;

    /**
     * @var AccountService
     */
    private $accountService;

    /**
     * @var CartService
     */
    private $cartService;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var CookieProviderInterface
     */
    private $cookieProvider;

    public function __construct(AccountLoginPageLoader $loginPageLoader, AccountService $accountService, CartService $cartService, Connection $connection, RequestStack $requestStack, CookieProviderInterface $cookieProvider)
    {
        $this->loginPageLoader = $loginPageLoader;
        $this->accountService = $accountService;
        $this->cartService = $cartService;
        $this->connection = $connection;
        $this->requestStack = $requestStack;
        $this->cookieProvider = $cookieProvider;
    }

    /**
     * @RouteScope(scopes={"storefront"})
     * @Route("/demo/data", name="frontend.demo.data", methods={"GET"}, defaults={"XmlHttpRequest": true})
     */
    public function getPwaCookies(Request $request, SalesChannelContext $salesChannelContext): JsonResponse
    {
        $postId = Uuid::randomHex();
        $translationId = Uuid::randomHex();
        $categoryId = Uuid::randomHex();
        $randomNumber = Random::getInteger(0, 100);
        $ascPostRepository = $this->container->get('asc_post.repository');
        $ascPostTranslationRepository = $this->container->get('asc_post_translation.repository');
        $ascCategoryRepository = $this->container->get('asc_category.repository');
        $ascCategoryTranslationRepository = $this->container->get('asc_category_translation.repository');
        $ascSceneCategoryRepository = $this->container->get('asc_post_category.repository');
        $ascPostCommentsRepository = $this->container->get('asc_post_comments.repository');
        $ascPostTagsRepository = $this->container->get('asc_post_tags.repository');
        $seoUrlsRepository = $this->container->get('seo_url.repository');
        $mediaRepository = $this->container->get('media.repository');

        $mediaCriteria = new Criteria();
        $mediaCriteria->addFilter(new EqualsFilter('fileName', 'scene_'. Random::getInteger(0, 6)));
        $mediaRaw = $mediaRepository->search($mediaCriteria, Context::createDefaultContext());
        $media = $mediaRaw->getEntities()->getIds();
        error_log(print_r(array('MEDIA', reset($media)), true)."\n", 3, '/app/error.log');

        //$sales_channel_id = '5dcb39875c004f94a62c596a3e9c5780';
        //$languageId = '2fbb5fe2e29a4D70aa5854ce7ce3e20b';
        $sales_channel_id = $salesChannelContext->getSalesChannel()->getId();
        $languageId= $salesChannelContext->getSalesChannel()->getDomains()->first()->getLanguageId();

        $featuredMediaId = '5f897f28cd9144f6a75e1b123e00e7ad';

                try {

                    $ascPostRepository->create([[
                        'id' => $postId,
                        'postAuthor' => '9d86c711e4894305ad7f9cf9a13307da',
                        'createdAt' => date('Y-m-d h:i:s'),
                        'postStatus' => "published",
                        'commentStatus' => "activated",
                        'postType' => "blog",
                        'featuredMediaId' => '5f897f28cd9144f6a75e1b123e00e7ad',
                    ]], Context::createDefaultContext());

                    $ascPostTranslationRepository->create([[
                        'id' => $translationId,
                        'postId' => $postId,
                        'languageId' => $languageId,
                        'createdAt' => date('Y-m-d h:i:s'),
                        'postContent' => "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                        'postTitle' => "Lorem Ipsum",
                        'postExcerpt' => "Lorem Ipsum is simply dummy text...",
                        'postSlug' => "lorem-ipsum",
                        'commentCount' => '2',
                    ]], Context::createDefaultContext());

                    $ascCategoryRepository->create([[
                        'id' => $categoryId,
                        'createdAt' => date('Y-m-d h:i:s'),
                        'mediaId' => $featuredMediaId,
                    ]], Context::createDefaultContext());

                    $ascCategoryTranslationRepository->create([[
                        'id' => $translationId,
                        'categoryId' => $categoryId,
                        'languageId' => $languageId,
                        'createdAt' => date('Y-m-d h:i:s'),
                        'categoryDescription' => "Category Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                        'categoryName' => "Category " . $randomNumber,
                        'categorySlug' => "category-" . $randomNumber,
                    ]], Context::createDefaultContext());

                    $ascSceneCategoryRepository->create([[
                        'id' => Uuid::randomHex(),
                        'postId' => $postId,
                        'categoryId' => $categoryId,
                        'createdAt' => date('Y-m-d h:i:s'),
                    ]], Context::createDefaultContext());

                    $ascPostCommentsRepository->create([[
                        'id' => Uuid::randomHex(),
                        'postId' => $postId,
                        'userId' => '9d86c711e4894305ad7f9cf9a13307da',
                        'languageId' => $languageId,
                        'active' => true,
                        'postComment' => "Comment Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
                        'createdAt' => date('Y-m-d h:i:s'),
                    ]], Context::createDefaultContext());

                    $ascPostTagsRepository->create([[
                        'id' => Uuid::randomHex(),
                        'postId' => $postId,
                        'languageId' => $languageId,
                        'tagName' => 'tag-'.$randomNumber,
                        'createdAt' => date('Y-m-d h:i:s'),
                    ]], Context::createDefaultContext());

                } catch (\Exception $e) {
                    error_log(print_r(array('$scenesRepositoryERRROR', $e->getMessage()), true)."\n", 3, '/app/error.log');
                }

        $criteria = new Criteria();
        //$criteria->addAssociation('media');
        //$criteria->addAssociation('user');
        $criteria->addAssociation('asc_post');
        $criteria->addAssociation('seoUrls');
        /*
                $res = $ascPostRepository->search($criteria, Context::createDefaultContext());
                $asc-posts = $res->getEntities();
                error_log(print_r(array('$asc-posts', $asc-posts), true)."\n"3, '/app/error.log');
        */
        //$res1 = $ascPostTranslationRepository->search($criteria, Context::createDefaultContext());
        //$postsTrans = $res1->getEntities();
        //error_log(print_r(array('$postsTrans', $postsTrans), true)."\n", 3, '/app/error.log');

        $cookieGroups = $this->cookieProvider->getCookieGroups();
        return new JsonResponse($cookieGroups);
    }

}
