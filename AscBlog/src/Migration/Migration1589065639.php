<?php declare(strict_types=1);

namespace AscBlog\Migration;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Shopware\Core\Framework\Migration\MigrationStep;

class Migration1589065639 extends MigrationStep
{
    public function getCreationTimestamp(): int
    {
        return 1589065639;
    }

    public function update(Connection $connection): void
    {
        try {
            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_post` (
                `id` BINARY(16) NOT NULL,
                `post_author` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `post_status` VARCHAR(255) NOT NULL,
                `comment_status` VARCHAR(255) NULL,
                `updated_at` DATETIME(3) NULL,
                `post_type` VARCHAR(255) NOT NULL,
                `featured_media_id` BINARY(16) NULL,
                `comment_count` VARCHAR(255) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.asc_post.featured_media_id` FOREIGN KEY (`featured_media_id`)
                REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post.post_author` FOREIGN KEY (`post_author`)
                REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_post_translation` (
                `asc_post_id` BINARY(16) NOT NULL,
                `language_id` BINARY(16) NOT NULL,
                `post_content` LONGTEXT NOT NULL,
                `post_title` VARCHAR(255) NOT NULL,
                `post_excerpt` LONGTEXT NULL,
                `post_slug` VARCHAR(255) NOT NULL,
                `meta_title` VARCHAR(255) NULL,
                `meta_description` MEDIUMTEXT NULL,
                `keywords` MEDIUMTEXT NULL,
                `seo_path` VARCHAR(255) NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`asc_post_id`, `language_id`),
                CONSTRAINT `fk.asc_post_translation.asc_post_id` FOREIGN KEY (`asc_post_id`)
                REFERENCES `asc_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_category` (
                `id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                `media_id` BINARY(16) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.asc_category.media_id` FOREIGN KEY (`media_id`)
                REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_category_translation` (
                `asc_category_id` BINARY(16) NOT NULL,
                `category_name` VARCHAR(255) NOT NULL,
                `category_slug` VARCHAR(255) NOT NULL,
                `category_description` LONGTEXT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                `language_id` BINARY(16) NOT NULL,
                PRIMARY KEY (`asc_category_id`, `language_id`),
                CONSTRAINT `fk.asc_category_translation.asc_category_id` FOREIGN KEY (`asc_category_id`)
                REFERENCES `asc_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_category_translation.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_post_category` (
                `post_id` BINARY(16) NOT NULL,
                `category_id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`post_id`, `category_id`),
                CONSTRAINT `fk.asc_category.category_id` FOREIGN KEY (`category_id`)
                REFERENCES `asc_category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post_category.post_id` FOREIGN KEY (`post_id`)
                REFERENCES `asc_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_post_comments` (
                `id` BINARY(16) NOT NULL,
                `post_id` BINARY(16) NOT NULL,
                `user_id` BINARY(16) NOT NULL,
                `language_id` BINARY(16) NOT NULL,
                `active` TINYINT(1) NULL DEFAULT '0',
                `post_comment` LONGTEXT NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.asc_post_comments.post_id` FOREIGN KEY (`post_id`)
                REFERENCES `asc_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post_comments.user_id` FOREIGN KEY (`user_id`)
                REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post_comments.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_post_tags` (
                `id` BINARY(16) NOT NULL,
                `post_id` BINARY(16) NOT NULL,
                `language_id` BINARY(16) NOT NULL,
                `tag_name` VARCHAR(255) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.asc_post_tags.post_id` FOREIGN KEY (`post_id`)
                REFERENCES `asc_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post_tags.language_id` FOREIGN KEY (`language_id`)
                REFERENCES `language` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

            $connection->exec("CREATE TABLE IF NOT EXISTS `asc_post_media` (
                `id` BINARY(16) NOT NULL,
                `post_id` BINARY(16) NOT NULL,
                `media_id` BINARY(16) NOT NULL,
                `created_at` DATETIME(3) NOT NULL,
                `updated_at` DATETIME(3) NULL,
                PRIMARY KEY (`id`),
                CONSTRAINT `fk.asc_post_media.post_id` FOREIGN KEY (`post_id`)
                REFERENCES `asc_post` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                CONSTRAINT `fk.asc_post_media.media_id` FOREIGN KEY (`media_id`)
                REFERENCES `media` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;");

        } catch (DBALException $e) {
            error_log(print_r(array('$dbTablesERRROR', $e->getMessage()), true)."\n", 3, '/app/error.log');
        }
    }

    public function updateDestructive(Connection $connection): void
    {
    }
}
